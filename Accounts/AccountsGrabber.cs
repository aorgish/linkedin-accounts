﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text.RegularExpressions;

namespace Accounts
{
    public abstract class AccountsGrabber
    {
       protected WebClient webClient = new WebClient()
          {
              // Кешировать запросы, чтобы при отладке получать страницу прямо из кеша, если она уже была когда-то скачана.
              CachePolicy = new RequestCachePolicy(RequestCacheLevel.CacheIfAvailable),
          };

        public IEnumerable<string> GetAccountLinks(string rootUri)
        {
            var rootLinks = GetRootLinks(rootUri);
            return GrabAccountLinks(rootLinks);
        }


        protected IEnumerable<string> GetRootLinks(string rootUri)
        {
            var content = GetContent(rootUri);
            return ParseRootLinks(content);
        }


        protected IEnumerable<string> GrabAccountLinks(IEnumerable<string> rootList)
        {
            var q = new Stack<string>(rootList.Reverse());
            while (q.Count > 0)
            {
                var link = q.Pop();
                var content = GetContent(link);

                foreach (var dirLink in ParseDirLinks(content).Reverse())
                    q.Push(dirLink);

                foreach (var accountLink in ParseAccountLinks(content))
                    yield return accountLink;
            }
        }


        protected abstract IEnumerable<string> ParseRootLinks(string content);

        protected abstract IEnumerable<string> ParseDirLinks(string content);

        protected abstract IEnumerable<string> ParseAccountLinks(string content);


        protected IEnumerable<string> ParseLinks(string content, Regex regex, string format)
        {
            return regex.Matches(content)
                        .Cast<Match>()
                        .Select(x => string.Format(format, x.Groups["hRef"].Value));
        }

        protected string GetContent(string link)
        {
            webClient.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0";
            return webClient.DownloadString(link);
        }
    }




    public class LinkedInGrabber : AccountsGrabber
    {
        protected Regex rootRegex    = new Regex(@"href=""(?<hRef>http://www.linkedin.com/directory/people/[a-z@]\.html)""");
        protected Regex dirRegex     = new Regex(@"href=""(?<hRef>/directory/people/[^""]+)"" title=");
        protected Regex accountRegex =  new Regex(@"href=""(?<hRef>/pub/[^""]+)"" title=");

        protected override IEnumerable<string> ParseRootLinks(string content)
        {
            return ParseLinks(content, rootRegex, "{0}");
        }


        protected override IEnumerable<string> ParseDirLinks(string content)
        {
            return ParseLinks(content, dirRegex, @"http://www.linkedin.com{0}");
        }

        
        protected override IEnumerable<string> ParseAccountLinks(string content)
        {
            return ParseLinks(content, accountRegex, @"http://www.linkedin.com{0}");
        }
    }




    // Facebook не совсем честно работает, сайт начинает выдавать страницу каталога со ссылкой на саму себя, чтобы зациклить анонимных ботов
    // Как обойти эту ситуацию, пока не знаю.
    public class FacebookGrabber : AccountsGrabber
    {
        protected Regex rootRegex    = new Regex(@"href=""(?<hRef>http://www.facebook.com/directory/people/([A-Z]|([1-9]{1,2})))""");
        protected Regex dirRegex     = new Regex(@"<li class=""fbDirectoryBoxColumnItem""><a href=""(?<hRef>http://www.facebook.com/directory/people/\S+)"">");
        protected Regex accountRegex = new Regex(@"<li class=""fbDirectoryBoxColumnItem""><a href=""(?<hRef>http://([^.]+).facebook.com/([^//]+))"">");

        protected override IEnumerable<string> ParseRootLinks(string content)
        {
            return ParseLinks(content, rootRegex, "{0}");
        }

        protected override IEnumerable<string> ParseDirLinks(string content)
        {
            return ParseLinks(content, dirRegex, "{0}");
        }

        protected override IEnumerable<string> ParseAccountLinks(string content)
        {
            return ParseLinks(content, accountRegex, "{0}");
        }
    }
}
