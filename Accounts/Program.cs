﻿using System;
using System.IO;

namespace Accounts
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 0;
            var grabber = new LinkedInGrabber();

            using (var writer = File.CreateText("Accounts.txt"))
            {
                foreach (var link in grabber.GetAccountLinks(@"https://www.linkedin.com/nhome/"))
                {
                    writer.WriteLine(link);
                    Console.WriteLine("{0}: {1}", ++counter, link);
                }
            }
            Console.WriteLine("Done!");
        }
    }
}
